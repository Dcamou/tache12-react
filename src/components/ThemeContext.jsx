import React, { createContext, useContext, useState } from 'react';
// Création d'un contexte pour stocker le thème
const ThemeContext = createContext();

// Création du fournisseur de thème
export const ThemeProvider = ({ children }) => {
    const [theme, setTheme] = useState({
        couleurPrincipale: 'Rouge',
        police: 'Arial',
});

  // Fonction pour mettre à jour le thème
    const updateTheme = (newTheme) => {
    setTheme(newTheme);
};

    return (
        <ThemeContext.Provider value={{ theme, updateTheme }}>
        {children}
        </ThemeContext.Provider>
    );
};

// Création d'un hook pour accéder au thème
export const useTheme = () => {
    const context = useContext(ThemeContext);
    if (!context) {
    throw new Error('useTheme doit être utilisé dans un ThemeProvider');
    }
    return context;
};