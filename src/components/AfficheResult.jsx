import React from 'react';
import { useTheme } from './ThemeContext';
import '../styles/AfficheResult.css'

function AfficheResult() {
    const { theme, updateTheme } = useTheme();

    const changeTheme = () => {
      // Mettre à jour les propriétés du thème
      updateTheme({
          couleurPrincipale: 'Bleu',
          police: 'Times New Romain',
      });
  };

  return (
    <>
        <div className='Imj-Affiche'>
            <p className='text-center'>Couleur Principale : {theme.couleurPrincipale}</p>
            <p>Police : {theme.police}</p>
            <button onClick={changeTheme} className='Imj-Button'>Change le Thème</button>
        </div>
    </>
  );
}

export default AfficheResult;
