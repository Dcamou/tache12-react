import React from 'react';
import { ThemeProvider } from './ThemeContext';
import AfficheResult from './AfficheResult';
import '../styles/App.css'

function App() {
    return (
        <ThemeProvider>
            <AfficheResult/>
        </ThemeProvider>
    );
}

export default App;
